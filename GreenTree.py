#!/usr/bin/python3
#Written by Forrest Hooker, 03/05/2023
#This is a simple random MAC generator that follows the same idealogy of PhreakGen - but with more potential sorting involved.
#
#A brief overview of MACs (Port this over to the ReadMe so we're not taking up space):
#
#[o1] [o2] [o3] [o4] [o5] [o6] 
# XX : XX : XX : YY : YY : YY
# ^^   ^^   ^^   ^^   ^^   ^^
# ||   ||   ||   ||   ||   ||
# ||   ||   ||   ||   ||   ||
# ||   ||   || [Extended identifer] (Basically, a serial number - or think about it like the last octet in an IPv4 address)
# ||   ||   ||
# ||   ||   ||
# ||   ||   ||
#[OUI/Vendor ID] (The string of bytes allocated by the IEEE to a vendor as an identifier - so, the thing that says "I am created by X Company")
# ||
# ||
# VV
#[b7] [b6] [b5] [b4] [b3] [b2] [b1] [b0] <-(The 8-Bits of the first Octet within the OUI half of the MAC address)
#                               ^^   ^^
#                               ||   ||
#                               ||  [Unicast: this bit will equal 0]
#                               ||  [Multicast: this bit will equal 1]
#                               ||
#                              [Globally Administered (OUI ENFORCED): This bit will equal 0]
#                              [Locally Administered: This bit will equal 1].
#

from scapy.all import *
from collections import defaultdict
#Use for debug, remove when done 
from time import sleep
#Use for hex generation
import random
#Use for sys.argv
import sys
#Use for optarg parsing
import getopt

## Vars/Arrs ##

gtHeader = ("GreenTree v1.0 - (https://gitlab.com/forrest_h1/python-scripts/networking-and-scapy-tools/greentree)")

## JSON Vars/Arrs ##

#Define ouiResources as us opening the oui.json
#ouiResources = csv.reader(open('resources/oui.csv', mode='r'))
#define ouiVendors as the data inside the json itself
#ouiVendors = {}


## Scapy Vars/Arrs ##
ifaceList = list(scapy.interfaces.get_if_list())
junkMACs = ['lo', 'docker'] 


## Hex Vars/Arrs ##

#Define all possible hex values
hexDigits= "0123456789ABCDEF"
#Define all hex values comprising a Unicast, Globally Administered MAC
indivGlob=("0","4","8","C")
#Define all hex values comprising a Unicast, Locally Administered MAC
indivLocal=("2","6","A","E")
#Define all hex values comprising a Multicast, Globally Administered MAC
multiGlob=("1","5","9","D")
#Define all hex values comprising a Multicast, Locally Administered MAC
multiLocal=("3","7","B","F")

## Empty Vars ##

#set userVendor as empty, to be set by User's arguments
userVendor=""
#set ouiString as empty, to be set by __ouiGen__()
ouiString=""
 

#Set color class for printing (Delete Vars as needed)
class color:
    RESET = '\033[0m'
    BOLD = '\033[1m'
    U_LINE = '\033[4m'
    GREEN = '\033[32m'
    RED = '\033[31m'
    YELLOW = '033[33m'
    WHITE = '\033[37m'
    CYAN = '\033[36m'
    MAGENTA = '\033[35m'
    BLUE = '\033[34m'
    #Combo Vars (Testing new way of defining these
    B_U_LINE= ('{}{}'.format(BOLD,U_LINE))
    B_GREEN = ('{}{}'.format(BOLD,GREEN))
    B_RED = ('{}{}'.format(BOLD,RED))
    B_YELLOW = ('{}{}'.format(BOLD,YELLOW))
    B_WHITE = ('{}{}'.format(BOLD,WHITE))
    B_CYAN = ('{}{}'.format(BOLD,CYAN))
    B_MAGEN = ('{}{}'.format(BOLD,MAGENTA))
    B_BLUE = ('{}{}'.format(BOLD,BLUE))


#Set symb class for printing symbols    
class symb:
    chk = '\u2713'
    warn = '\u26A0'
    



def __Main__():
    __argCheck__()
    __usrMAC__()
    __ouiGen__()
    __extIDGen__()
    __macPrint__()



#Parse user argument (If given), set vars accordingly
def __argCheck__():
    global userVendor
    #If no user argument given, generate genuinely random MAC address.
    print("\n{}{}{}".format(color.WHITE,gtHeader,color.RESET))
    if len(sys.argv) == 1:
        print("{}\nGenerating random MAC Address. (Use -v [vendor_string] for specific OUI){}".format(color.GREEN,color.RESET))
        userVendor = False
        return userVendor
    #Else, run opt arg checks.
    else:
        argv = sys.argv[1:]
        try:
            #Set valid opts (for now) to "-v" and "-h".
            opts, args = getopt.getopt(argv, "v:h")
            for opt, arg in opts:
                if opt in ['-h']:
                    print("Usage coming soon!")
                elif opt in ['-v']:
                    print(arg, "is in values")
                    sleep(2000)
        #Do lame exception handling (I swear this could be better)
        except getopt.GetoptError:
            print("\nInvalid option used. Valid arguments are -v [vendor_string] and -h")
            sys.exit()
    #print(userVendor)

def __usrMAC__():
    global ifaceDict
    ifaceDict = {}
    for iFace in ifaceList:
        MAC = get_if_hwaddr(iFace)
        ifaceDict[iFace] = MAC
    return {}

#Generate an OUI [(XX:XX:XX:)yy:yy:yy] based on whether or not user wants a random OUI or a genuine vendor's OUI
def __ouiGen__():
    global ouiString
    #If no user argument given, generate completely random OUI string.
    if userVendor == False:
        ouiList = []
        #Fix this doofus, you know how indexes work.
        for byte in range (1,7):
            #define bit as a random choice of bit from hexDigits.
            ##bit will always equal a string type because of how hexDigits was defined.
            bit = random.choice(hexDigits)
            ouiList.append(bit)
            #If the current byte generated is the 2nd in the first octet, check to see if it matches the Unicast/Administered lists
            if byte == 2:
                print("")
                if bit in indivGlob:
                    byteMsg = ("Globally Administered Unicast MAC address selected.")
                elif bit in indivLocal:
                    byteMsg = ("Locally Administered Unicast MAC address selected.")
                elif bit in multiGlob:
                    byteMsg = ("Globally Administered Multicast MAC address selected.")
                elif bit in multiLocal:
                    byteMsg = ("Locally Administered Multicast MAC address selected.")
                else:
                    print(type(bit))
                print("{}{} {}".format(color.BLUE,byteMsg,color.RESET))
        octetBegin = [0,1,2,3,4,5,6,7]
        #Remove any None-types from ouiList
        for i in octetBegin:
            byteIndex = (i+2)
            ouiList[i:byteIndex] = [''.join(ouiList[i:byteIndex])]
            ouiList = list(filter(None, ouiList))
        #Join OUI List vals
        ouiHex = "".join(ouiList)
        #if ouiHex in ouiVendors.keys():
         #   print(ouiVendors[ouiHex])
        #ouiString = ":".join(ouiList)
    #elif userVendor in ouiVendors:
        #for vendor in ouiVendors.keys():
            #if userVendor in ouiVendors[userVendor]:
             #   ouiHex = ouiVendor[userVendor]
             #   break
        #ouiString = ':'.join(ouiHex[i:i+2] for i in range (0, len(ouiHex), 2))
        #print(ouiString)
        #sleep(20000)

#Generates the last 3 octets of a MAC Address. Always random. Would be annoying to make it not random. :)
def __extIDGen__():
    global externIDString
    extIDList = []
    #add to extIDList with looping range that selects random hexDigits.
    for idByte in range (1,7):
        idBit = random.choice(hexDigits)
        extIDList.append(idBit)
    idOctetBegin = [0,1,2,3,4,5,6,7]
    #List join stuff VVV
    for j in idOctetBegin:
        idByteIndex = (j+2)
        extIDList[j:idByteIndex] = [''.join(extIDList[j:idByteIndex])]
        extIDList = list(filter(None, extIDList))
    externIDString = ":".join(extIDList)

def __macPrint__():
    #Predefine message to save some horizontal space
    addrMsg=("Generated MAC Address:")
    genMAC=("{}:{}".format(ouiString,externIDString))
    existMsg=("    Current MAC Address(es)     ")
    print("\n{}{}{}".format(color.B_U_LINE,existMsg,color.RESET))
    for i in ifaceDict.keys():
        if len(i) != 7:
            str1 = ("{:>7}".format(i))
        print("{}{} - {}{}{}{}".format(color.B_CYAN,str1,color.RESET,color.CYAN,ifaceDict[i],color.RESET))

    print("\n{}{} {}{}\n{}".format(color.WHITE,addrMsg,color.B_U_LINE,genMAC,color.RESET))
    

__Main__()
