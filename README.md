# GreenTree



GreenTree is, in it's current stage, a Random MAC Address generator with the intention of being used to spoof interfaces. Rather than just generate random hex bits, GreenTree was written to identify if random values selected determine the OUI to be a Unicast vs Multicast address, and whether or not it's just created a Globally Administered or Locally Administered address. At the time of writing this, implementation of the ieee's oui.txt file available from their site is underway.



## Required Modules



`random`



`scapy` - Not necessary at this time, but will be with v1.2



`sys`



`getopt`



# How does it work?

If you're familiar with MAC Addresses, feel free to skip to usage.


Every network interface, be it Wifi, Ethernet, Bluetooth, etc. has an assigned MAC Address that looks something like this:


```
[o1] [o2] [o3] [o4] [o5] [o6] 
 XX : XX : XX : YY : YY : YY
 ^^   ^^   ^^   ^^   ^^   ^^
 ||   ||   ||   ||   ||   ||
 ||   ||   ||   ||   ||   ||
 ||   ||   || [Extended identifer] (Basically, a serial number - or think about it like the last octet in an IPv4 address)
 ||   ||   ||
 ||   ||   ||
 ||   ||   ||
[OUI/Vendor ID] (The string of bytes allocated by the IEEE to a vendor as an identifier - so, the thing that says "I am created by X Company")
 ||
 ||
 VV
[b7] [b6] [b5] [b4] [b3] [b2] [b1] [b0] <-(The 8-Bits of the first Octet within the OUI half of the MAC address)
                               ^^   ^^
                               ||   ||
                               ||  [Unicast: this bit will equal 0]
                               ||  [Multicast: this bit will equal 1]
                               ||
                              [Globally Administered (OUI ENFORCED): This bit will equal 0]
                              [Locally Administered: This bit will equal 1].

```


Without giving too much of a lecture you probably already know, your MAC address is used by pretty much everything router related on a network - ARP especially, which assigns an IP based upon your MAC. While the Extended Identifier functions more like the last octet of an IP address, the OUI can not only identify the vendor of the interface (and thus be used to determine the device with that interface), but it can also determine whether or not the MAC is assigned to a Multicast or Unicast interface - and whether or not the MAC itself is Globally Administered (As in, assigned by a Vendor from the factor), or Locally Administered (Assigned by an admin in a network to quickly identify interfaces). 



What GreenTree actually does is attempt to generate both a believable OUI (At least in terms of Hex Values correlating to types of casting and administration) and a random Extended identifier. In its current stage, it cannot identify and select a Vendor to use, and it cannot assign a MAC for a given interface. These are planned for v1.1 and v1.2 respectively.



# Usage (For Now):



Simply clone the repo, run a `chmod u+x GreenTree.py`, and then run `./GreenTree.py`.



